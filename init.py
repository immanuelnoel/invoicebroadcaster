#! python

import sys
import time
import getopt
import threading

# Import project libraries
import database
from operations import Operations


# Used to limit the number of threads at any point in time
maxThreadsOnSystem = 30

# System Start - Entry point
def init(argv):
    
    maxThreads = 0
    testEnvironment = False
    helpString = "init.py -m <MaxThreadCount> \nOptional Parameters: \nTest Environment: -t"
    
    # Validate parameters
    try:
    
        opts, args = getopt.getopt(argv,"tm:")
        for opt, arg in opts:
        
            if opt == '-h':
                print(helpString)
                return
                
            if opt == '-m':
                maxThreads = int(arg)
                print("Attempting to run script with " + str(maxThreads) + " threads")
                
            if opt == '-t':
                testEnvironment = True
                print("Attempting to run script on test environment")
    
    except getopt.GetoptError:
        print(helpString)
        return
        
    # Invoke spawnThreads
    result = spawnThreads(maxThreads, testEnvironment)
    print("Time taken to execute: " + str(result["TimeTaken"]) + " seconds")
    
    return

# Spawns thread to do the actual invocation, while complying to the maxThreads specified. Returns a dict with status and time taken to execute in seconds
def spawnThreads(maxThreads, testEnvironment):        
        
    startTime = time.time()    
    
    # Use maxThreadsOnSystem defined globally
    global maxThreadsOnSystem
        
    if maxThreads != 0:
        
         # Limit maxThreads to 30 to stay low on system resources. Must be lesser than maximum DBConnections that can be created at a time
        if maxThreads < maxThreadsOnSystem and maxThreads < database.maxConnections:
           
            # Handle Test Environment
            fileName = "customerID.txt"
            if(testEnvironment == True):
                fileName = "customerID-test.txt"
            
            
            # Read CustomerID from file and trigger a new thread to process
            # Use at most MaxThreads  
            oppr = Operations()
            
            with open(fileName, "r") as file:
            
                customerIndex = 0
                for line in file:
                
                    # Run if the number of threads spawned is within the alloted boundary. Else wait
                    while 1:
                        
                        if threading.activeCount() - 1 < maxThreads:
                    
                            CustomerID = str.rstrip(line)
                            customerIndex = customerIndex + 1
                            
                            print("Spawning thread " + str(threading.activeCount()) + " to process " + str(customerIndex) + " CustomerID, " + CustomerID)
                            
                            # Spawn thread
                            t = threading.Thread(target=oppr.processInvoice, args=(CustomerID,))
                            t.start()
                            break
                            
                        else:
                            time.sleep(1)
            
            
            # Wait until all threads have executed. 
            # This loop would not run for the entire length of the program, but would start after the last thread has been invoked. Hence would loop for the time taken to process the last invoice (1 second)
            while threading.activeCount() != 1:
                continue
                
            result = {'Success': True, 'TimeTaken': int(time.time() - startTime)}
                
        else:
             print("Error: ThreadCount greater than maxDBConnections available or manual limit. Reduce number of threads and try again")
             result = {'Success': False, 'TimeTaken': int(time.time() - startTime)}
    
    else:
        print("'-m' not specified, or is 0. Try '-h' for help")
        result = {'Success': False, 'TimeTaken': int(time.time() - startTime)}
    
    # Return result dict
    return result

    
if __name__ == "__main__": 
    init(sys.argv[1:])