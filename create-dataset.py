
import random
import string

customerIDsToGenerate = 500

# Attempt to create alphanumeric strings with len(20) for the number of customerIDsToGenerate specified
data = string.ascii_lowercase + string.digits
with open("customerID-test.txt", "w") as fout:
    for i in range(customerIDsToGenerate):
        x = ''.join(random.choice(data) for n in range(20))
        print(x, file=fout)