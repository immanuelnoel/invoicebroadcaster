# DATABASE

from invoice import Invoice

maxConnections = 1000
currentConnections = 0

# Allow 'maxConnections' connections at any time
def getDBConnection():
   
    global currentConnections 
    global maxConnections 
   
    cnx = None
    
    if(currentConnections < maxConnections):
        currentConnections = currentConnections + 1
        cnx = {"index": currentConnections}
    
    return cnx
    

# Close connections once the operation is complete
# Free's up connections to be used by subsequent operations    
def closeDBConnection(cnx):
    
    global currentConnections 
    
    returnValue = False
    if(currentConnections > 0):
        currentConnections = currentConnections - 1
        returnValue = True
    
    return returnValue
    
    
# Get Invoice Details from DB - Mock
def getInvoice(cnx, CustomerID):
    
    global maxConnections 
    
    # Validate cnx object
    invoiceObj = None
    if(cnx != None and cnx['index'] > 0 and cnx['index'] < maxConnections + 1):
       invoiceObj = Invoice(CustomerID)
        
    return invoiceObj
    


