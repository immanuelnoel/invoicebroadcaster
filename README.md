# Invoice Broadcaster       
          
Sends invoices to customerIDs mentioned in a text document, spawning a maximum of -m threads     
      
### Usage      
    
Operation  | Command      
------------- | -------------      
Generate Test Data  | python create-dataset.py      
Execute  | python init.py -m <Number Of Threads (Limited to the number of DBConnections available)>. To send out 50,000 invoices in an hour, 15 threads would be sufficient      
Execute Test  | python init.py -t -m <Number Of Threads (Limited to the number of DBConnections available)>         
      
### Variables      
     
Variable  | Location      
------------- | -------------      
Sample CustomerIDs  | Edit 'customerIDsToGenerate' in create-dataset.py     
DB Connection Limit  | Edit 'maxConnections' in database.py    
No. of threads system can handle. Default: 30  | Edit 'maxThreadsOnSystem' in init.py    
         
### Source Description       
        
File  | Description      
------------- | -------------      
create-dataset.py  | Python script to generate sample CustomerIDs      
customerID.txt  | Sample CustomerIDs data       
customerID-test.txt  | Test version of the above file        
init.py  | Entry point. Spawns a maximum of '-m' threads to process invoice         
operations.py  | Business logic. Processes invoice         
database.py  | Mock DB Class       
invoice.py  | Mock Invoice Class      
              
### Tests       
Unit / Concurrency tests exist in the below files       
* tests_init.py    
* tests_concurrency.py      
        
### Trial Runs       
50,000 invoices were sent out in 3387 seconds (57 minutes) when run with 15 threads       
              