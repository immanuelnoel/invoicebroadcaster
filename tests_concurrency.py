import unittest

# Unit Tests
# Tests for concurrency

import init

class concurrency(unittest.TestCase):
    
    # Test run time for processing 500 invoices (customerID-test.txt) with a maximum of 10 database connections
    
    # Test with 10 threads 
    def test1(self):
        
        result = init.spawnThreads(10, True)
        
        self.assertTrue(result["Success"])
        self.assertLess(result["TimeTaken"], 60)
    
    # Test with 5 threads
    def test2(self):
        
        result = init.spawnThreads(5, True)
        
        self.assertTrue(result["Success"])
        self.assertLess(result["TimeTaken"], 120)
        
     # Test with 20 threads 
    def test3(self):
        
        result = init.spawnThreads(20, True)
        
        self.assertTrue(result["Success"])
        self.assertLess(result["TimeTaken"], 30)    
        
if __name__ == '__main__':
    unittest.main()