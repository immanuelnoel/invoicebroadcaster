import unittest

# Unit Tests
# Tests for init.py

import init

class tests_init(unittest.TestCase):
    
    # Test large number of threads
    def test_neg1(self):
        
        result = init.spawnThreads(100, True)
        self.assertFalse(result["Success"])
    
   # Test threads greater than maximum database connections
    def test_neg2(self):
        
        result = init.spawnThreads(2000, True)
        self.assertFalse(result["Success"])
        
        
if __name__ == '__main__':
    unittest.main()