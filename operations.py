#! python

import time

# Local Import
import database

class Operations:
    
    # Attempts to process invoice
    # Attempts to acquire a database connection and process invoice. If all db connections are occupied, wait for a second (average invoice process time) and try again
    def processInvoice(self, CustomerID):

        # DB Connection
        # If DB Connection not available, wait for one second
        cnx = database.getDBConnection()
        if cnx == None:
            
            # Wait until a connection is available
            print("Connection unavailable. Waiting...")
            time.sleep(1)
            self.processInvoice(CustomerID)
        
        else:
            
            # Process  Invoice
            print("DBConnection index: " + str(cnx['index']))
            
            # Get Customer ID    
            # CustomerID = self.getCustomerID()
            print("Processing invoice for CustomerID, " + str(CustomerID))
            
            # Prepare Invoice
            result = self.prepareInvoice(cnx, CustomerID)
            print(result)
            
            database.closeDBConnection(cnx)


    # Gets the invoice from DB, and sends an email if database returns a valid Invoice Object
    def prepareInvoice(self, cnx, CustomerID):
        
        invoice = database.getInvoice(cnx, CustomerID)
        
        # Processing something with the invoice for one second
        time.sleep(1)
        
        result = "Invoice could not be sent" 
        if invoice != None:
            result = self.emailInvoice(invoice)
            
        return result
        
        
    # Send an email with invoice
    def emailInvoice(self, invoice):
        return "Invoice to " + getattr(invoice, "CustomerName") + " (" + getattr(invoice, "Email") + ") emailed successfully!"
    